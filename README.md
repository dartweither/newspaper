# NewsPaper

### Django shell commmands

1. Create two users

```python
>>> from news.models import User
>>> User.objects.create_user('Vitaliy Arthas', password='suyda')
<User: Vitaliy Arthas>
>>> User.objects.create_user('Ocherednyara', password='pass')
<User: Ocherednyara>
```

2. Create two objects of author model

```python
>>> from news.models import Author, User
>>> Author.objects.create(user_id=User.objects.get(username='Vitaliy Arthas'), rating=0.0)
<Author: Author object (1)>
>>> Author.objects.create(user_id=User.objects.get(username='Ocherednyara'), rating=0.0)
<Author: Author object (2)>
```

3. Create four categories

```python
>>> from news.models import Category
>>> Category.objects.create(name='Art')
<Category: Category object (1)>
>>> Category.objects.create(name='Music')
<Category: Category object (2)>
>>> Category.objects.create(name='Programming')
<Category: Category object (3)>
>>> Category.objects.create(name='Physics')
<Category: Category object (4)>
```

4. Create two articles and news(uncountable?)
5. Add categories

```python
>>> from news.models import User, Author, Category, Post
>>> author1=Author.objects.get(user_id=User.objects.get(username='Ocherednyara'))
>>> author2=Author.objects.get(user_id=User.objects.get(username='Vitaliy Arthas'))
>>> category1=Category.objects.get(name='Art')
>>> category2=Category.objects.get(name='Music')
>>> category3=Category.objects.get(name='Physics')
>>> post1=Post.objects.create(author_id=author1, title='Papich Zhenilsya', body='Lol kupilis', news_or_article='news')
>>> post1.category.add(category1)
>>> post1.category.add(category2)
>>> post1.save()
>>> post2=Post.objects.create(author_id=author1, title='Chto-to umnoe', body='Real umnaya shtuka', news_or_article='article')
>>> post2.category.add(category3)
>>> post2.save()
>>> post3=Post.objects.create(author_id=author2, title='Chto-to poumnee', body='Real umnaya shtuka no drugaya', news_or_article='article')
>>> post3.category.add(category3)
>>> post3.save()
```

6. Make four comments
7. like() and dislike() some comments and posts

```python
>>> from news.models import User, Post, Comment
>>> user1=User.objects.get(username='Ocherednyara')
>>> user2=User.objects.get(username='Vitaliy Arthas')
>>> post1=Post.objects.get(title='Papich Zhenilsya')
>>> post2=Post.objects.get(title='Chto-to umnoe')
>>> post3=Post.objects.get(title='Chto-to poumnee')
>>> comment1=Comment.objects.create(user_id=user1, post_id=post1, body='Blin ya popalsya :(')
>>> comment2=Comment.objects.create(user_id=user2, post_id=post2, body='Realno umnoe')
>>> comment3=Comment.objects.create(user_id=user2, post_id=post1, body='Poumnee chem eto https://habr.com/ru/company/jugru/blog/340178/') 
>>> comment4=Comment.objects.create(user_id=user2, post_id=post1, body='Nadoelo')
>>> [comment1.like() for _ in range(33)]
>>> [comment2.dislike() for _ in range(13)]
```

8. Update users rating

Classes have auto-update methods

9. Output rating and username of the best rating user

```python
>>> from news.models import Author
>>> users=[u for u in Author.objects.all()]
>>> users.sort(key=lambda x: x.rating, reverse=True)
>>> users[0].user_id
<User: Ocherednyara>
>>> users[0].rating
165.0
```

10. Output date of publishing of the best post
11. Also output related comments

```python
>>> from news.models import Post, Comment
>>> posts=[p for p in Post.objects.all()]
>>> posts.sort(key=lambda x: x.rating, reverse=True)
>>> posts[0].rating
44.0
>>> posts[0].publishing_time
datetime.datetime(2021, 11, 23, 3, 46, 6, 318515, tzinfo=<UTC>)
>>> posts[0].author_id.user_id
<User: Ocherednyara>
>>> posts[0].title
'Papich Zhenilsya'
>>> posts[0].preview()
'Lol kupilis'
>>> comments=[c for c in Comment.objects.filter(post_id=posts[0])]
>>> for c in comments:
...     print(c.publishing_time, c.user_id.username, c.rating, c.body, sep=',')
... 
2021-11-23 11:47:51.010906+00:00,Ocherednyara,0.0,Blin ya popalsya :(
2021-11-23 11:48:07.896615+00:00,Vitaliy Arthas,0.0,Poumnee chem eto https://habr.com/ru/company/jugru/blog/340178/
2021-11-23 11:48:41.839067+00:00,Vitaliy Arthas,0.0,Nadoelo
```
