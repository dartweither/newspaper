from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Author(models.Model):
    """Author class"""
    user_id = models.OneToOneField(User, on_delete=models.CASCADE)
    rating = models.FloatField(default=0.0)
    
    def update_rating(self, increment):
        self.rating += increment

class Category(models.Model):
    """Category class"""
    name = models.CharField(max_length=100, unique=True)

class Post(models.Model):
    """Post class"""
    author_id = models.ForeignKey(Author, on_delete=models.CASCADE)
    news_or_article = models.CharField(
        choices=(
            ('NEWS', 'news'),
            ('ARTICLE', 'article')
        ),
        max_length=100
    )
    publishing_time = models.DateTimeField(auto_now_add=True)
    category = models.ManyToManyField(Category, through='PostCategory')
    title = models.CharField(max_length=100)
    body = models.TextField()
    rating = models.FloatField(default=0.0)

    def like(self):
        """Increase post rating by 1"""
        self.rating += 1.0
        self.save()
        self._update(1.0)

    def dislike(self):
        """Decrease post rating by 1"""
        self.rating -= 1.0
        self.save()
        self._update(-1.0)

    def preview(self):
        """Return first 124 symbols of post body"""
        if len(self.body) > 124:
            return self.body[:125] + '...'
        else:
            return self.body

    def _update(self, increment):
        """"Update author rating"""
        self.author_id.update_rating(3 * increment)
        self.author_id.save()

class PostCategory(models.Model):
    """PostCategory class"""
    post_id = models.ForeignKey(Post, on_delete=models.CASCADE)
    category_id = models.ForeignKey(Category, on_delete=models.CASCADE)

class Comment(models.Model):
    """Comment class"""
    post_id = models.ForeignKey(Post, on_delete=models.CASCADE)
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    body = models.TextField()
    publishing_time = models.DateTimeField(auto_now_add=True)
    rating = models.FloatField(default=0.0)

    def like(self):
        """Increase comment rating by 1"""
        self.rating += 1.0
        self._update(1.0)

    def dislike(self):
        """Decrease comment rating by 1"""
        self.rating -= 1.0
        self._update(-1.0)

    def _update(self, increment):
        """Update user rating"""
        author = Author.objects.filter(user_id=self.user_id)[0]
        author.update_rating(increment)
        author.save()
